extern crate gio;
extern crate glib;
extern crate gtk;

use gtk::prelude::*;
use gtk::{AboutDialog, ApplicationWindow, Builder, Button, Label, MenuItem, MenuItemExt};

pub fn new_window() -> ApplicationWindow {
  let builder: Builder = Builder::new_from_resource("/org/gnome/HelloRust/gtk/window.ui");

  let window: ApplicationWindow = builder
    .get_object("app_window")
    .expect("Application window could not be loaded");

  window.connect_delete_event(|_, _| {
    gtk::main_quit();
    gtk::Inhibit(false)
  });

  let hello_label: Label = builder.get_object("label").expect("label not found");
  let btn_action: Button = builder
    .get_object("action_button")
    .expect("action_button not found");
  btn_action.connect_clicked(move |_| hello_label.set_text("Action button clicked"));

  let mnu_about: MenuItem = builder
    .get_object("about_menu")
    .expect("about_menu not found");

  {
    let window = window.clone();
    mnu_about.connect_activate(move |_| {
      show_about(&window);
    });
  }

  return window;
}

fn show_about(window: &ApplicationWindow) {
  let builder: Builder = Builder::new_from_resource("/org/gnome/HelloRust/gtk/about.ui");

  let about_dialog: AboutDialog = builder
    .get_object("about_dialog")
    .expect("about_dialog not found");
  about_dialog.set_modal(true);
  about_dialog.set_transient_for(window);
  about_dialog.show();
}
